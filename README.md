# FrontEnd-Ionic 3

## Run your application


Download the code from: https://msanchez90@bitbucket.org/mariano_myself/frontend-ionic.git

At the console put: npm install

Then you can run it with: npm ionic serve


## Testing

For testing we use Karma and Jasmine.

Run the tests with: npm test

