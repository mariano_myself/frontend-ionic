import { Gnome } from './gnome';

export class Brastlewark {
  gnomes: Gnome[];
  
  constructor(values: Object = {}) {
    Object.assign(this, values);
  }
}
