import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnomeProfilePage } from './gnome-profile';

@NgModule({
  declarations: [
    GnomeProfilePage,
  ],
  imports: [
    IonicPageModule.forChild(GnomeProfilePage),
  ],
})
export class GnomeProfilePageModule {}
