import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { Gnome } from '../../model/gnome';

@IonicPage()
@Component({
  selector: 'page-gnome-profile',
  templateUrl: 'gnome-profile.html',
})
export class GnomeProfilePage {
  gnome: Gnome;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.gnome = navParams.get('gnome');
  }
  
}
