import { Component, OnInit } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';

import { Gnome } from '../../model/gnome';
import { BrastlewarkProvider } from '../../providers/brastlewark/brastlewark';
import { GnomeSearchPage } from '../gnome-search/gnome-search';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage implements OnInit{
  gnomesInfo:Gnome[]=[];
  employeesPercent: number;
  friendlyPercent: number;
  ageAverage: number;
  weightAverage: number;
  heightAverage: number;

  constructor(public navCtrl: NavController,
    public brastlewarkProvider: BrastlewarkProvider,
    private alertCtrl: AlertController) { }

    ngOnInit(): void {
      this.initInformation();
    }

    async initInformation() {
      try {
        this.gnomesInfo = await this.brastlewarkProvider.getGnomesInfo();
        !this.gnomesInfo ? this.showErrorAlert() : this.calculateStadistics();
      }
      catch {
        this.showErrorAlert();
      }
    }

    calculateStadistics() {
      try {
        let employees = this.gnomesInfo.filter(gnome => gnome.professions.length != 0);
        this.employeesPercent = Math.round(employees.length / this.gnomesInfo.length * 100);
        let friendlies = this.gnomesInfo.filter(gnome => gnome.friends.length != 0);
        this.friendlyPercent = Math.round(friendlies.length / this.gnomesInfo.length * 100);

        this.ageAverage = Math.round(this.gnomesInfo
          .map(gnome => gnome.age)
          .reduce((a,b) => a+b,0)
          / this.gnomesInfo.length);
        this.weightAverage = Math.round(this.gnomesInfo
          .map(gnome => gnome.weight)
          .reduce((a,b) => a+b,0)
          / this.gnomesInfo.length);
        this.heightAverage = Math.round(this.gnomesInfo
          .map(gnome => gnome.height)
          .reduce((a,b) => a+b,0)
          / this.gnomesInfo.length);
      }
      catch (e){
        console.error(e.message);
        console.error('Error using the data provided by the API');
      }
    }

  search() {
    this.navCtrl.push(GnomeSearchPage,{gnomesInfo: this.gnomesInfo},{animate:false});
  }

  showErrorAlert() {
    let alert = this.alertCtrl.create({
      title: 'Connection Error',
      subTitle: 'We cannot access to the data. Please try in a few minutes.',
      buttons: ['Ok']
    });
    alert.present();
  }
}
