import { async, TestBed } from '@angular/core/testing';
import { IonicModule } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { HomePage } from './home.ts';
import { NgCircleProgressModule, CircleProgressOptions } from 'ng-circle-progress';
import { NavController } from 'ionic-angular';
import { BrastlewarkProvider } from '../../providers/brastlewark/brastlewark';
import { HttpClient, HttpHandler } from '@angular/common/http';

describe('Home Component', () => {
  let homePage;
  let component;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [HomePage],
      imports: [
        IonicModule.forRoot(HomePage),
        NgCircleProgressModule
      ],
      providers: [
        StatusBar,
        SplashScreen,
        NavController,
        BrastlewarkProvider,
        HttpClient,
        HttpHandler,
        CircleProgressOptions
      ]
    })
  }));
  beforeEach(() => {
    homePage = TestBed.createComponent(HomePage);
    component = homePage.componentInstance;
  });
  it ('should be created', () => {
    expect(component instanceof HomePage).toBe(true);
  });

  describe('Test for Calculate Stadistics', () => {
    it('should calculate Stadistics', () => {
      const mockAPIResponse = require('../../../testing/data.json');
      let dataError, dataResponse;

      component.gnomesInfo = mockAPIResponse;
      component.calculateStadistics();
      expect(component.employeesPercent).toEqual(88);
      expect(component.friendlyPercent).toEqual(88);
      expect(component.ageAverage).toEqual(207);
      expect(component.weightAverage).toEqual(39);
      expect(component.heightAverage).toEqual(105);
    });

    it('should return an error', () => {
      const mockAPIResponse = {};
      component.gnomesInfo = mockAPIResponse;

      component.calculateStadistics();

      expect(component.employeesPercent).toBeUndefined();
    });

  });
});
