import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GnomeSearchPage } from './gnome-search';

@NgModule({
  declarations: [
    GnomeSearchPage,
  ],
  imports: [
    IonicPageModule.forChild(GnomeSearchPage),
  ],
})
export class GnomeSearchPageModule {}
