import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ViewChild } from '@angular/core';

import { Gnome } from '../../model/gnome';
import { GnomeProfilePage } from '../gnome-profile/gnome-profile';

@IonicPage()
@Component({
  selector: 'page-gnome-search',
  templateUrl: 'gnome-search.html',
})
export class GnomeSearchPage {
  gnomesInfo:Gnome[]=[];
  gnomesFilter:Gnome[]=[];
  gnomesFilterOnlyByWords:Gnome[]=[];
  chooseTypeClicked: boolean=false;
  ageRange= {lower: 0, upper: 60};
  heightRange= {lower: 0, upper: 60};
  weightRange= {lower: 0, upper: 60};
  wordToSearch: String='';
  upperAge: number=0;
  lowerAge: number=0;
  upperHeight: number=0;
  lowerHeight: number=0;
  upperWeight: number=0;
  lowerWeight: number=0;
  hairColorSelected: string= 'All';
  hairColorFilter: string[]=['All','Black','Gray','Green','Pink','Red'];

  @ViewChild('searchBar') searchBar;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.gnomesInfo = navParams.get('gnomesInfo');
    this.initInformation();
  }

  initInformation() {
    this.upperAge = Math.ceil(this.gnomesInfo
      .map(gnome => gnome.age)
      .reduce((a,b) =>(a > b) ? a : b));
    this.lowerAge = Math.floor(this.gnomesInfo
      .map(gnome => gnome.age)
      .reduce((a,b) =>(a < b) ? a : b));
    this.upperHeight = Math.ceil(this.gnomesInfo
      .map(gnome => gnome.height)
      .reduce((a,b) =>(a > b) ? a : b));
    this.lowerHeight = Math.floor(this.gnomesInfo
      .map(gnome => gnome.height)
      .reduce((a,b) =>(a < b) ? a : b));
    this.upperWeight = Math.ceil(this.gnomesInfo
      .map(gnome => gnome.weight)
      .reduce((a,b) =>(a > b) ? a : b));
    this.lowerWeight = Math.floor(this.gnomesInfo
      .map(gnome => gnome.weight)
      .reduce((a,b) =>(a < b) ? a : b));
    this.ageRange = {lower: this.lowerAge, upper: this.upperAge};
    this.heightRange = {lower: this.lowerHeight, upper: this.upperHeight};
    this.weightRange = {lower: this.lowerWeight, upper: this.upperWeight};
  }

  ionViewDidLoad() {
    setTimeout(() => {
      this.searchBar.setFocus();
    },100);
  }

  goBack(){
    this.navCtrl.pop({animate:false});
  }

  searchByFilter() {
    if (this.hairColorSelected != 'All') {
      this.gnomesFilter = this.gnomesFilterOnlyByWords.filter((gnome) => {
        return (gnome.age >= this.ageRange.lower &&  gnome.age <= this.ageRange.upper &&
                gnome.weight >= this.weightRange.lower &&  gnome.weight <= this.weightRange.upper &&
                gnome.height >= this.heightRange.lower &&  gnome.weight <= this.heightRange.upper &&
                gnome.hair_color === this.hairColorSelected
              );
      })
    }
    else {
      this.gnomesFilter = this.gnomesFilterOnlyByWords.filter((gnome) => {
        return (gnome.age >= this.ageRange.lower &&  gnome.age <= this.ageRange.upper &&
                gnome.weight >= this.weightRange.lower &&  gnome.weight <= this.weightRange.upper &&
                gnome.height >= this.heightRange.lower &&  gnome.weight <= this.heightRange.upper
              );
      })
    }
  }

  async searchByWords(wordToSearch) {
    this.gnomesFilter=[];
    if (wordToSearch && wordToSearch.trim() != '') {
      this.gnomesFilterOnlyByWords = await this.gnomesInfo.filter((gnome) => gnome.name.toLowerCase().indexOf(wordToSearch.toLowerCase()) > -1);
      this.searchByFilter();
    }
  }

  refreshFilter() {
    this.searchByFilter();
  }

  refreshSearchBar(ev) {
    this.searchByWords(ev.target.value);
  }

  goToProfile(gnome) {
    this.navCtrl.push(GnomeProfilePage, {
      gnome: gnome
    });
  }

  showFilter() {
    if (this.chooseTypeClicked === undefined) {
      this.chooseTypeClicked = true;
    }
    else if (this.chooseTypeClicked === true){
      this.chooseTypeClicked = false;
    }
    else if (this.chooseTypeClicked === false){
      this.chooseTypeClicked = true;
    }
  }

}
