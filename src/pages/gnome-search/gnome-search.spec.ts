import { async, TestBed } from '@angular/core/testing';
import { IonicModule } from 'ionic-angular';
import { NavController, NavParams } from 'ionic-angular';

import { GnomeSearchPage } from './gnome-search.ts';

describe('Gnome Search Component', () => {
  let gnomeSearchPage;
  let component;
  const mockAPIResponse = require('../../../testing/data.json');
  const data = {gnomesInfo: mockAPIResponse};
  const navParams = new NavParams(data);
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GnomeSearchPage],
      imports: [
        IonicModule.forRoot(GnomeSearchPage)
      ],
      providers: [
        NavController,
        {provide: NavParams, useValue: navParams}
      ]
    })
  }));
  beforeEach(() => {
    gnomeSearchPage = TestBed.createComponent(GnomeSearchPage);
    component = gnomeSearchPage.componentInstance;
  });
  it ('should be created', () => {
    expect(component instanceof GnomeSearchPage).toBe(true);
  });

  describe('Test values for Filter', () => {
    it('should calculate values used by the Filter', () => {
      expect(component.ageRange).toEqual({lower:85, upper:306});
      expect(component.upperAge).toEqual(306);
      expect(component.lowerAge).toEqual(85);
      expect(component.heightRange).toEqual({lower:91, upper:128});
      expect(component.upperHeight).toEqual(128);
      expect(component.lowerHeight).toEqual(91);
      expect(component.weightRange).toEqual({lower:35, upper:44});
      expect(component.upperWeight).toEqual(44);
      expect(component.lowerWeight).toEqual(35);
    });
  });

  describe('Test search engine', () => {
    it('should find Malbin gnomes', async () => {
        const error = await component.searchByWords('malbin');
        expect(component.gnomesFilterOnlyByWords.length).toEqual(2);
        expect(component.gnomesFilter.length).toEqual(2);
      });
    it('should find Malbin Green gnomes', async () => {
        let error = await component.searchByWords('malbin');
        component.hairColorSelected = 'Green';
        error = await component.refreshFilter();
        expect(component.gnomesFilter.length).toEqual(1);
        expect(component.gnomesFilter[0].id).toEqual(2);
      });
      it('should find Malbin gnomes - Age between 166 and 239', async () => {
          let error = await component.searchByWords('malbin');
          component.hairColorSelected = 'All';
          component.ageRange = {lower:166, upper:239};
          error = await component.refreshFilter();
          expect(component.gnomesFilter.length).toEqual(1);
          expect(component.gnomesFilter[0].id).toEqual(2);
        });
      it('should find Malbin gnomes - Height between 106 and 107', async () => {
          let error = await component.searchByWords('malbin');
          component.hairColorSelected = 'All';
          component.heightRange = {lower:106, upper:107};
          error = await component.refreshFilter();
          expect(component.gnomesFilter.length).toEqual(1);
          expect(component.gnomesFilter[0].id).toEqual(2);
        });
      it('should find Malbin gnomes - Weight between 35 and 36', async () => {
          let error = await component.searchByWords('malbin');
          component.hairColorSelected = 'All';
          component.weightRange = {lower:35, upper:36};
          error = await component.refreshFilter();
          expect(component.gnomesFilter.length).toEqual(1);
          expect(component.gnomesFilter[0].id).toEqual(2);
        });
      it('should find Malbin gnomes - None', async () => {
          let error = await component.searchByWords('malbin');
          component.hairColorSelected = 'All';
          component.weightRange = {lower:32, upper:33};
          error = await component.refreshFilter();
          expect(component.gnomesFilter.length).toEqual(0);
        });
    });
  });
