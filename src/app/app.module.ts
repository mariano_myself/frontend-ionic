import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { HttpClientModule } from '@angular/common/http';
import { NgCircleProgressModule } from 'ng-circle-progress';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { GnomeProfilePage } from '../pages/gnome-profile/gnome-profile';
import { GnomeSearchPage } from '../pages/gnome-search/gnome-search';
import { BrastlewarkProvider } from '../providers/brastlewark/brastlewark';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    GnomeProfilePage,
    GnomeSearchPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpClientModule,
    NgCircleProgressModule.forRoot({
      radius: 100,
      outerStrokeWidth: 16,
      innerStrokeWidth: 8,
      outerStrokeColor: "#78C000",
      innerStrokeColor: "#C7E596",
      animationDuration: 300,
      showSubtitle: false,
      showUnits: true,
      responsive: true,
      titleFontSize: "50",
      unitsFontSize: "50",
      unitsColor: "#FFFFFF",
      titleColor: "#FFFFFF"
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    GnomeProfilePage,
    GnomeSearchPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    BrastlewarkProvider
  ]
})
export class AppModule {}
