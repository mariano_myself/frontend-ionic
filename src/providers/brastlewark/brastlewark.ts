import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Brastlewark } from '../../model/brastlewark';
import { Gnome } from '../../model/gnome';

@Injectable()
export class BrastlewarkProvider {
  apiUrl = 'https://raw.githubusercontent.com/rrafols/mobile_test/master/data.json';

  constructor(public httpClient: HttpClient) { }

  async getGnomesInfo(): Promise<Gnome[]> {
    try {
      let response = await this.httpClient.get<Brastlewark>(this.apiUrl).toPromise();
      return response['Brastlewark'];
    }
    catch (e) {
      console.error(e.message);
      console.error('Error trying to invoke API service');
    }
  }

}
